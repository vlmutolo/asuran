# Hall of Fame

This is a list of all individuals that have contributed to Asuran. They have improved the project
through their time and effort.

Thank you. 

## Code Contributors

These fine individuals have taken valuable time out of their day to make Asuran better. 

- [@aidanhs](https://gitlab.com/aidanhs)

## Bug Hunters

These people have had the unfortunate experience of encountering a bug in Asuran, but stepped up to
the plate, and filed a report that (hopefully) enabled us to fix the bug.

- [@aidanhs](https://gitlab.com/aidanhs)
